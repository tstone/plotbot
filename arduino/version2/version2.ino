/*
    Plot program for the arduino based plotbot
    Copyright (C) 2018  Tim Sander <plotbot@krieglstein.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Servo.h>
#include <inttypes.h>
#include <stdlib.h>
#include <avr/sleep.h>
class StepperMotor {
private:
  //const int steps[8] = {8,12,4,6,2,3,1,9};
  const int steps[8] = {1,3,2,6,4,12,8,9};
  int m_pins[4];
  int8_t m_dir;
  uint32_t m_tps;
  uint32_t m_timer;
  uint32_t m_stepCounter;
  uint8_t m_stepCycle;
  uint8_t m_timeWarp;
public:
  StepperMotor(int p1,int p2, int p3, int p4) { //Konstruktor
    m_pins[0]=p1;
    m_pins[1]=p2;
    m_pins[2]=p3;
    m_pins[3]=p4;
    m_stepCounter=0;
    m_stepCycle=0;
    m_tps=1;
    m_dir=1;
    m_timeWarp=10;
    for(int i=0;i<4;i++) pinMode(m_pins[i],OUTPUT);
  }

  uint32_t getStepCount() {
    return m_stepCounter;
  }

  void setTimeWarp(uint8_t timeWarp) {
    if(timeWarp=0) timeWarp=1;
    m_timeWarp=timeWarp;
  }

  void setTPS(uint32_t tps) {
    m_timer=0;
    m_tps=tps;
    m_stepCounter=0;
  }

  void timeStep() {
    m_timer+=m_timeWarp;
    if(m_timer>=m_tps) {
      m_timer=0;
      m_stepCounter++;
      doStep();
    }
  }

  void doStep() {
    m_stepCycle = ((m_stepCycle + m_dir) % 8) & 0x7;
    outputStep();
  }

  void setDirection(int8_t dir) {
    m_dir = dir;
    if(dir>1) dir=1;
    else if (dir<-1) dir=-1;
    outputStep();
  }

  int8_t getDirection() {
    return m_dir;
  }

  void invertDirection() {
    m_dir = -m_dir;
  }

  void powerdown() {
    for(int i=0;i<4;i++) digitalWrite(m_pins[i], LOW);
  }

  void outputStep() {
    int8_t currentStep = steps[m_stepCycle];
    for(int i=0,o=1;i<4;i++,o<<=1) {
      digitalWrite(m_pins[i], currentStep&o?HIGH:LOW);
    }
  }

};

class Controller {
private:
  StepperMotor ml;
  StepperMotor mr;
  StepperMotor mpen;
  const int32_t stepw = 254L; //µm Pi/360* Raddurchmesser * 5,625°/64 * 10 (for precision)
  const int32_t rb = 106150L; // 150770L; //µm Abstand der Räder von der Mitte
  const int stepsPlay = 27;
  const int32_t motorDelay = 1000L;
  const int penStartUp = 400;
  const int penStartDown = 180;
  const int penSteps = 220;

  bool usePen;
  bool autoPen;

public:
  Controller(): ml(0,1,2,3)
			   ,mr(7,6,5,4)  //backwards to have the motor turn counterwise
			   ,mpen(8,9,10,11)
      {
        usePen = false;
        autoPen = true;
        mpen.setDirection(1);
        for(int i=0;i<penStartUp;i++) {
          mpen.doStep();
          delayMicroseconds(motorDelay+100);
        }
        mpen.setDirection(-1);
        for(int i=0;i<penStartDown;i++) {
          mpen.doStep();
          delayMicroseconds(motorDelay+100);
        }
      }

  void setAutoPen(bool on) {
    autoPen = on;
  }

  void powerdown() {
    ml.powerdown();
    mr.powerdown();
    mpen.powerdown();
  }

  void paint(bool on) {
    if(usePen ^ on) {
      movePen(on);
    }
    usePen=on;
  }

  void movePen(bool up) {
    mpen.setDirection(up?-1:1);
    for(int i=0;i<penSteps;i++) {
      mpen.doStep();
      delayMicroseconds(motorDelay);
    }
    mpen.powerdown();
  }

  void line(int32_t micrometer) {
    checkDirection(micrometer>0,micrometer>0);
    if(micrometer<0) micrometer=-micrometer;
    int32_t stepsNeeded = (micrometer*10L)/stepw;
    for(long int i=0;i<stepsNeeded;i++) {
      ml.doStep();
      mr.doStep();
      delayMicroseconds(motorDelay);
    }
  }

  void lineMM(int32_t milimeter) {
    line(milimeter*1000);
  }

  void forward(long int mm) {
    line(mm*1000L);
  }

  void backward(long int mm) {
    line(mm*-1000L);
  }

  void timeStepDelay() {
    delayMicroseconds(1);
  }

  void turn(long int deg) { //clockwise
    turnD(deg*100);
  }

  void turnD(int64_t deg) { //clockwise/100
    int64_t stepsForTurn = (rb * 3141L)/100L/stepw;
    if(autoPen && usePen) movePen(false);
    if(deg==0) return;
    if(deg<0) {
      checkDirection(false,true);
      deg = -deg;
    } else if(deg>0) checkDirection(true,false);
    int64_t steps = (stepsForTurn * deg)/36000L;
    for(int i=0;i<steps;i++) {
      ml.doStep();
      mr.doStep();
      delayMicroseconds(motorDelay); 
    }
    if(autoPen && usePen) movePen(true);
  }
  
  void circle(int32_t radius,int angle) {
    circleUm(radius*1000,angle);
  }

  void circleUm(int32_t radiusum,long angle) { //negativer Winkel dreht gegen Uhrzeigersinn
    int64_t leftum;
    int64_t rightum;
    /*
      int32_t roundingError = angle/45L;
     angle+=roundingError;*/
    if(angle>0) {
      leftum = rb/2+radiusum;
      rightum = radiusum-rb/2l;
    }
    else {
      rightum = rb/2+radiusum;
      leftum = radiusum-rb/2;
      angle=-angle;
    }
    checkDirection(leftum>0,rightum>0);
    leftum = labs(leftum);
    rightum= labs(rightum);
    int64_t stepsToGo;
    int64_t radius;
    StepperMotor *m;
    if(leftum>rightum) {
      ml.setTPS(1000L);
      uint64_t n = 1000000L/rightum;
      mr.setTPS((leftum*n)/1000L);
      radius = (leftum*3142L)/1000L;
      radius *= angle;
      radius /= 180L;
      stepsToGo = (radius*10L)/stepw;
      m=&ml;
    }
    else {
      mr.setTPS(1000L);
      uint64_t n = 1000000L/leftum;
      ml.setTPS((rightum*n)/1000L);
      radius = (rightum*3142L)/1000L;
      radius *= angle;
      radius /= 180L;
      stepsToGo = (radius*10L)/stepw;
      m=&mr;
    }
    while(m->getStepCount()<stepsToGo) {
      ml.timeStep();
      mr.timeStep();
      timeStepDelay();
    }
  }

  void checkDirection(bool left,bool right) {
    bool l=false;
    bool r=false;
    l = left?ml.getDirection()<0:ml.getDirection()>0;
    if(l) ml.invertDirection();
    r = right?mr.getDirection()<0:mr.getDirection()>0;
    if(r) mr.invertDirection();
    if(l or r) {
      for(int i=0;i<stepsPlay;i++) {
        if(l) ml.doStep();
        if(r) mr.doStep();
        delayMicroseconds(motorDelay);
      }
    }
  }

  void beep() {
    for(int i=0;i<3000;i++) {
      ml.doStep();
      mr.doStep();
    }
  }

  void finish(bool doBeep) { //does never return;
    paint(false);
    if(doBeep) beep();
    powerdown();

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_cpu(); //does not seem to work?
    for(;;);
  }

  void circleSpeed(uint8_t s) {
    ml.setTimeWarp(s);
    mr.setTimeWarp(s);
  }
};

/* Und hier wird der Roboter instanziiert */
Controller robbi;


void setup() {
  robbi.paint(false);
  robbi.setAutoPen(true);
}

void A(int32_t x) {
  robbi.turnD(-7596);
  robbi.paint(true);
  robbi.line(x*1237);
  robbi.turnD(-2807);
  robbi.line(-x*1237);
  robbi.paint(false);
  robbi.line(x*309);
  robbi.turnD(10404);
  robbi.paint(true);
  robbi.line(-x*450);
  robbi.paint(false);
  robbi.turnD(2564);
  robbi.line(x*693);
  robbi.turnD(-2564);
}

void B(int32_t x) {
  robbi.turnD(-9000);
  robbi.line(x*75);
  robbi.turnD(12781);
  robbi.paint(true);
  robbi.circleUm(x*375,-218);
  robbi.line(x*144);
  robbi.paint(false);
  robbi.turnD(963);
  robbi.line(-x*343);
  robbi.turnD(13443);
  robbi.paint(true);
  robbi.circleUm(x*300,-196);
  robbi.paint(false);
  robbi.turnD(975);
  robbi.line(-x*102);
  robbi.turnD(-4756);
  robbi.paint(true);
  robbi.line(x*1122);
  robbi.paint(false);
  robbi.turn(9295);
  robbi.line(x*589);
  robbi.turnD(-295);
}

void C(int32_t x) {
  robbi.turnD(-6270);
  robbi.line(x*1199);
  robbi.turnD(-6085);
  robbi.paint(true);
  robbi.circleUm(x*300,-146);
  robbi.line(x*600);
  robbi.circleUm(x*320,-146);
  robbi.paint(false);
  robbi.turnD(12851);
  robbi.line(x*215);
  robbi.turnD(-6233);
}  
 
void D(int32_t x) {
  robbi.turn(90);
  robbi.line(-x*75);
  robbi.turnD(-4996);
  robbi.paint(true);
  robbi.circleUm(x*320,-130);
  robbi.line(x*580);
  robbi.circleUm(x*300,-140);
  robbi.paint(false);
  robbi.turnD(946);
  robbi.line(-x*98);
  robbi.turnD(-4950);
  robbi.paint(true);
  robbi.line(x*1129);
  robbi.paint(false);
  robbi.turnD(-8709);
  robbi.line(x*552);
  robbi.turnD(-291);
}

void E(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.turn(90);
  robbi.line(x*500);
  robbi.paint(false);
  robbi.turn(-45);
  robbi.line(-x*707);
  robbi.turn(45);
  robbi.paint(true);
  robbi.line(x*300);
  robbi.paint(false);
  robbi.turnD(-6680);
  robbi.line(-x*762);
  robbi.turnD(6680);
  robbi.paint(true);
  robbi.line(x*500);
  robbi.paint(false);
  robbi.line(x*100);  
}

void lF(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.turn(90);
  robbi.line(x*500);
  robbi.paint(false);
  robbi.turn(-45);
  robbi.line(-x*707);
  robbi.turn(45);
  robbi.paint(true);
  robbi.line(x*300);
  robbi.paint(false);
  robbi.turnD(-6680);
  robbi.line(-x*762);
  robbi.turnD(6680);
  robbi.paint(false);
  robbi.line(x*600);
}

void G(int32_t x) {
  robbi.turnD(-6270);
  robbi.line(x*1199);
  robbi.turnD(-6085);
  robbi.paint(true);
  robbi.circleUm(x*300,-146);
  robbi.line(x*600);
  robbi.circleUm(x*300,-180);
  robbi.paint(false);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(-x*150);
  robbi.paint(false);
  robbi.turnD(5023);
  robbi.line(x*391);
  robbi.turnD(-5023);
}

void H(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.line(-x*500);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(x*500);
  robbi.turn(90);
  robbi.line(-x*500);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.turn(-90);
  robbi.line(x*100);
}

void I(int32_t x) {
  robbi.paint(true);
  robbi.line(x*100);
  robbi.paint(false);
  robbi.line(-x*50);
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.turn(90);
  robbi.line(-x*50);
  robbi.paint(true);
  robbi.line(x*100);
  robbi.paint(false);
  robbi.turnD(8524);
  robbi.line(x*1204);
  robbi.turnD(-8524);
}

void J(int32_t x) {
  robbi.turn(90);
  robbi.line(-x*75);
  robbi.turnD(-3000);
  robbi.paint(true);
  robbi.circleUm(x*150,-150);
  robbi.line(x*1050);
  robbi.paint(false);
  robbi.turnD(-572);
  robbi.line(-x*1206);
  robbi.turnD(9572);
}

void K(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.line(-x*1050);
  robbi.turnD(2546);
  robbi.paint(true);
  robbi.line(x*1163);
  robbi.paint(false);
  robbi.line(-x*994);
  robbi.turnD(11000);
  robbi.paint(true);
  robbi.line(x*425);
  robbi.paint(false);
  robbi.turnD(-4546);
  robbi.line(x*229); 
}

void L(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.line(-x*1200);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(x*300);
  robbi.paint(false);
  robbi.line(x*100);
}

void M(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.turnD(-1834);
  robbi.line(-x*949);
  robbi.turnD(3687);
  robbi.line(x*949);
  robbi.turnD(-1834);
  robbi.line(-x*1200);
  robbi.paint(false);
  robbi.turn(90);
  robbi.line(x*100);
}

void N(int32_t x) {
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.turnD(-2657);
  robbi.line(-x*1342);
  robbi.turnD(2657);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.line(-x*1200);
  robbi.turn(90);
  robbi.line(x*100);
}

void O(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*320);
  robbi.paint(true);
  robbi.line(x*560);
  robbi.circleUm(x*320,180);
  robbi.line(x*560);
  robbi.circleUm(x*320,180);
  robbi.paint(false);
  robbi.turnD(11339);
  robbi.line(x*806);
  robbi.turnD(-2339);
}

void P(int32_t x) {
  robbi.line(x*75);
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1157);
  robbi.paint(false);
  robbi.turnD(4747);
  robbi.line(-x*99);
  robbi.turnD(-947);
  robbi.paint(true);
  robbi.circleUm(x*300,261);
  robbi.paint(false);
  robbi.turnD(1814);
  robbi.line(-x*849);
  robbi.turnD(13079);
}

void Q(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*320);
  robbi.paint(true);
  robbi.line(x*560);
  robbi.circleUm(x*320,180);
  robbi.line(x*560);
  robbi.circleUm(x*320,180);
  robbi.paint(false);
  robbi.turnD(10415);
  robbi.line(x*441);
  robbi.turnD(3084);
  robbi.paint(true);
  robbi.line(x*300);
  robbi.paint(false);
  robbi.turn(-45);
  robbi.line(x*100);
}

void R(int32_t x) {
  robbi.line(x*75);
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1157);
  robbi.paint(false);
  robbi.turnD(4749);
  robbi.line(-x*99);
  robbi.turnD(-947);
  robbi.paint(true);
  robbi.circleUm(x*300,261);
  robbi.paint(false);
  robbi.turnD(-982);
  robbi.line(-x*82);
  robbi.turnD(3488);
  robbi.paint(true);
  robbi.line(-x*719);
  robbi.paint(false);
  robbi.turnD(12187);
  robbi.line(x*100);
}

void S(int32_t x) {
  robbi.turn(90);
  robbi.line(-x*160);
  robbi.turnD(-3000);
  robbi.paint(true);
  robbi.circleUm(x*320,-240);
  robbi.circleUm(x*280,240);
  robbi.paint(false);
  robbi.turnD(2049);
  robbi.line(x*1075);
  robbi.turnD(-8049);
}

void T(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*1200);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(x*600);
  robbi.paint(false);
  robbi.line(-x*300);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.turn(-90);
  robbi.line(x*400);
}

void U(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*1200);
  robbi.paint(true);
  robbi.line(-x*900);
  robbi.circleUm(x*-300,-180);
  robbi.line(-x*900);
  robbi.paint(false);
  robbi.turnD(-476);
  robbi.line(x*1204);
  robbi.turnD(-8523);
}

void V(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*1200);
  robbi.turnD(-1404);
  robbi.paint(true);
  robbi.line(-x*1237);
  robbi.turnD(2807);
  robbi.line(x*1237);
  robbi.paint(false);
  robbi.turnD(-1879);
  robbi.line(-x*1204);
  robbi.turnD(9476);
}

void W(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*1200);
  robbi.paint(true);
  robbi.line(-x*1200);
  robbi.turnD(2320);
  robbi.line(x*762);
  robbi.turnD(-4640);
  robbi.line(-x*762);
  robbi.turnD(2320);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.turnD(-476);
  robbi.line(-x*1204);
  robbi.turnD(9476);
}

void X(int32_t x) {
  robbi.turnD(-6343);
  robbi.paint(true);
  robbi.line(x*1342);
  robbi.paint(false);
  robbi.turnD(6343);
  robbi.line(-x*600);
  robbi.turnD(6343);
  robbi.paint(true);
  robbi.line(x*1342);
  robbi.paint(false);
  robbi.turnD(-6343);
}
  
void Y(int32_t x) {
  robbi.line(x*300);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(-x*700);
  robbi.turnD(-3096);
  robbi.line(-x*583);
  robbi.paint(false);
  robbi.line(x*583);
  robbi.turnD(6193);
  robbi.paint(true);
  robbi.line(-x*583);
  robbi.paint(false);
  robbi.turnD(-3573);
  robbi.line(x*1204);
  robbi.turnD(-8524);  
}  
  
void Z(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*1200);
  robbi.turn(90);
  robbi.paint(true);
  robbi.line(x*600);
  robbi.turnD(-6343);
  robbi.line(-x*1342);
  robbi.turnD(6343);
  robbi.line(x*600);
  robbi.paint(false);
  robbi.line(x*100);
}  

void n1(int32_t x) {
  robbi.line(x*212);
  robbi.turn(-90);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.turn(45);
  robbi.line(-x*300);
  robbi.paint(false);
  robbi.turnD(11747);
  robbi.line(x*1036);
  robbi.turnD(-7247);
}

void n2(int32_t x) {
  robbi.turn(90);
  robbi.line(-x*899);
  robbi.paint(true);
  robbi.circleUm(x*-300,217);
  robbi.line(-x*899);
  robbi.turn(53);
  robbi.line(x*600);
  robbi.paint(false);
  robbi.line(x*100);
}

void n3(int32_t x) {
  robbi.turnD(2040);
  robbi.paint(true);
  robbi.circleUm(x*320,-200);
  robbi.line(x*75);
  robbi.paint(false);
  robbi.line(-x*75);
  robbi.paint(true);
  robbi.circleUm(x*-290,-203);
  robbi.paint(false);
  robbi.turnD(9167);
  robbi.line(x*1292);
  robbi.turnD(-6571);
}

void n4(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*700);
  robbi.turnD(1457);
  robbi.paint(true);
  robbi.line(x*517);
  robbi.paint(false);
  robbi.line(-x*517);
  robbi.turnD(7543);
  robbi.paint(true);
  robbi.line(x*475);
  robbi.paint(false);
  robbi.line(-x*75);
  robbi.turn(90);
  robbi.line(-x*500);
  robbi.paint(true);
  robbi.line(x*1200);
  robbi.paint(false);
  robbi.turn(-90);
  robbi.line(x*100);
}

void n5(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*75);
  robbi.turnD(12821);
  robbi.paint(true);
  robbi.circleUm(x*350,-218);
  robbi.line(x*144);
  robbi.turn(90);
  robbi.line(x*452);
  robbi.turn(90);
  robbi.line(x*350);
  robbi.paint(false);
  robbi.turnD(7804);
  robbi.line(x*1178);
  robbi.turnD(-7804);
}

void n6(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*300);
  robbi.paint(true);
  robbi.circleUm(x*300,360);
  robbi.line(x*600);
  robbi.circleUm(x*300,140);
  robbi.paint(false);
  robbi.turnD(3115);
  robbi.line(x*1106);
  robbi.turnD(-8115); 
}

void n7(int32_t x) {
  robbi.turnD(-7157);
  robbi.paint(true);
  robbi.line(x*1265);
  robbi.turnD(7157);
  robbi.line(-x*400);
  robbi.paint(false);
  robbi.turnD(7556);
  robbi.line(x*568);
  robbi.turnD(-7556);
  robbi.paint(true);
  robbi.line(x*150);
  robbi.paint(false);
  robbi.turnD(7223);
  robbi.line(x*683);
  robbi.turnD(-7223);
}

void n8(int32_t x) {
  robbi.line(x*340);
  robbi.paint(true);
  robbi.circleUm(x*340,-360);
  robbi.paint(false);
  robbi.turnD(-7648);
  robbi.line(x*661);
  robbi.turnD(4548);
  robbi.paint(true);
  robbi.circleUm(x*300,-299);
  robbi.paint(false);
  robbi.turnD(1623);
  robbi.line(x*876);
  robbi.turnD(-4724);
}

void n9(int32_t x) {
  robbi.turn(-90);
  robbi.line(x*121);
  robbi.turn(-40);
  robbi.paint(true);
  robbi.circleUm(x*-340,-140);
  robbi.line(-x*560);
  robbi.circleUm(x*300,360);
  robbi.paint(false);
  robbi.turnD(-634);
  robbi.line(x*906);
  robbi.turnD(-8366);
}

void testLine() {
  robbi.paint(true);
  robbi.forward(5);
  robbi.paint(false);
}

typedef void (*letterfunc)(int32_t x);
letterfunc bigletters[26] = {A,B,C,D,E,lF,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z};
letterfunc numbers[10] = {O,n1,n2,n3,n4,n5,n6,n7,n8,n9};

void string(char* i,int32_t s) {
  char *c = i;
  while(*c!=NULL) {
    int offset = *c;
    if(offset>=65 && offset<90) {
      bigletters[offset-65](s);
    } else if(offset>=48 && offset<58) {
      numbers[offset-48](s);
    } else if(*c==' ') robbi.line(s*300);
    c++;
  }
}

/* Die wichtigsten Befehle:
 
 paint(false);    Stift hochnehmen
 paint(true);   Stift absetzen
 setAutoPen(x);   wenn x=true dann beim "turn" Befehl automatisch den Stift hochnehmen
 turn(grad);      um "grad" Drehen (negativ = gegen Uhrzeigersinn)
 turnD(grad/100); um "grad/100 drehen (negativ = gegen Uhrzeigersinn)
 line(µm);      um µm bewegen (negativ ist rückwärts)
 lineMM(mm);    um mm bewegen (negativ ist rückwärt)
 forward(mm);   vorwärts Fahren um "mm" Millimeter
 backward(mm);    rückwärts Fahren um "mm" Millimeter
 circle(radius,grad) ein Kreis Zeichnen mit radius in Millimeter und dem Winkel grad (-360 0 360)
 finish(beep);    Stehenbleiben wenn beep = true dann Piepsen
 
 for(int i=0;i<100;i++) {} Schleife, alles wird 100 mal wiederholt
 if(bedingung) {} else {}  Verzweigung wenn Bedingung wahr dann wird alles im ersten Block gemacht, ansonsten alles im zweiten
*/
 
void loop() {
  robbi.paint(true);
  int32_t g=20;
  string("FROHE WEIHNACHT",g);
  robbi.finish(true);
}
